#include <iostream>

using namespace std;

//ESTRUCTURA EN LA QUE SE ALMACENA LA INFORMACION Y CUAL ES EL IZQUIERDO O DERECHO DE UN NODO
struct NODO{ int info;
           NODO *izq, *der;};

class ARBOL{


NODO *raiz;

public:	ARBOL(){
  //EN UN COMIENZO LA RAIZ SE INICIALIZA EN NULL
  raiz=NULL;
  }

	NODO *raiz_arbol() {return raiz;}
	int ins_arbol (int);
	NODO *buscar_arbol (int, NODO *, NODO *);
	void borrar_nodo(NODO *, NODO *);
	NODO *buscar(int, NODO **, NODO *);
	int retira_arbol(int);
	void inorden(NODO *);
	void preorden(NODO *);
	void posorden(NODO *);
	void destruir (NODO *p);
	~ARBOL();};

//BUSQUEDA DE UN ELEMENTO EN UN ARBOL
NODO *ARBOL::buscar_arbol(int n, NODO *raiz, NODO *elemento){
  //SI NO HAY RAIZ SE RETORNARA EL ELEMENTO A BUSCAR
  if (raiz==NULL)
		return elemento;

//SI EL NUMERO A BUSCAR ES MENOR
if (n < raiz->info){
	elemento=raiz;
	elemento=buscar_arbol(n,raiz->izq,elemento);
	return elemento;
}else
	if (n > raiz->info) {
		elemento=raiz;
		elemento=buscar_arbol(n,raiz->der,elemento);
		return elemento;}
    else
		return NULL;}

//INSERTAR NUEVO ELEMENTO EN EL ARBOL
int ARBOL:: ins_arbol(int n){
	NODO *q;
  //SI ES EL PRIMER ELEMENTO SERA LA RAIZ
	if (raiz == NULL){
		raiz= new NODO;
		raiz->info =n;
		raiz->izq=raiz->der=NULL;
		return 0;}

q =buscar_arbol(n,raiz,NULL);
  //SI NO HAY UNA RAIZ O ERROR DE OTRO TIPO
	if (q==NULL) return -1;
		NODO *nuevo;
		nuevo= new NODO;
    //SE AGREGA EL VALOR A GUARDAR
		nuevo->info = n;
    //EL NODO NO VA A TENER HIJOS
		nuevo->izq=nuevo->der=NULL;


		if (n<q->info)
        	q->izq=nuevo;
		else   q->der=nuevo;
			return 0;}

void ARBOL::borrar_nodo (NODO *q, NODO *p){
NODO  *r, *s,*t;
if (p->izq==NULL) r=p->der;
else if (p->der==NULL) r=p->izq;
       else {s =p;
  r=p->der;
  t=r->izq;
  while(t!=NULL){s=r; r=t; t=t->izq;}
if (p!=s){
	    s->izq=r->der;
    r->der=p->der; }
r->izq =p->izq;
}
	if (q==NULL) raiz= r;
	else if (p==q->izq) q->izq =r;
	       else q->der = r;
	delete p;}

NODO *ARBOL::buscar(int n, NODO **p, NODO *q){
	if (*p==NULL) return NULL;
	if (n <(*p)->info){
	          q=*p;
	           *p=(*p)->izq;
	          q=buscar(n,p,q);
	          return q;}
	else
		if (n>(*p)->info){
		q=*p;
		*p=(*p)->der;
		q =buscar(n,p,q);
		return q;}
	       else return q;}

int ARBOL:: retira_arbol(int n){
	NODO *q, *p=raiz;
	cout << "buscando..."<<endl;
	q=buscar(n,&p,NULL);
	if (p==NULL) return -1;
	borrar_nodo(q,p);
	return 0;}

void ARBOL::inorden(NODO *p){
	if (p!=NULL){
        inorden(p->izq);
        cout<<p->info<<endl;
        inorden(p->der);}}

void ARBOL::preorden(NODO *p){
	if (p!=NULL){
		cout<<p->info<<endl;
        preorden(p->izq);
        preorden(p->der);}}

void ARBOL::posorden(NODO *p){
if (p!=NULL){
        posorden(p->izq);
        posorden(p->der);
        cout<<p->info<<endl; }}

void ARBOL::destruir(NODO *p){
	if (p!=NULL){
	    destruir(p->izq);
	    destruir(p->der);
	    delete p;

ARBOL::~ARBOL() {destruir(raiz);}

int main(){
	ARBOL a; NODO *p;  int i;

  //SE PERMITE AGREGAR ELEMENTOS MIENTRAS NO SE INGRESE UN -69
	while(i!=-69){
    cout<< "Introduzca un numero para agregar o -69 para retirar un numero: ";
    cin>>i;
		if (a.ins_arbol(i)==-1) cout<<"Ya existe"<< endl;
  }
  //Se permite ingresar al while
  i=0;
	while(i!=-69){
      cout<<"Número a retirar (para finalizar el programa ingrese -69) ...";
      cin>>i;
    	if (a.retira_arbol(i)==-1)cout<<"No existe"<<endl;
  }


  cout<< "Resultados"<<endl;
	cout<<"inOrden" << endl;
	a.inorden(a.raiz_arbol());
	 cout<<"preOrden" << endl;
	a.preorden(a.raiz_arbol());
	 cout<<"posOrden"<< endl;
	a.posorden(a.raiz_arbol());

}
